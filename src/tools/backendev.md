# Sviluppo backend

Prima di tutto bisogna sistemare l'ambiente di sviluppo installando Java, MariaDB e le dependencies di Spring Boot per lo sviluppo del backend di MyFidelity.

**Installazione di Java e MariaDB**

Bisogna installare in base al proprio sistema operativo gli strumenti necessari:

<div style="text-align:center"><br><img src="../img/winos.png" alt="Winget Logo" width=75vw height=75vh /><br></div>

- **[Winget](https://winstall.app/) (Windows 10/11)**
    ```
    winget install --id=Oracle.JDK.21  -e
    ```
    ```
    winget install --id=MariaDB.Server  -e
    ```

<div style="text-align:center"><br><img src="../img/brewpm.png" alt="Homebrew Logo" width=200vw height=125vh /><br></div>

- **[Homebrew](https://formulae.brew.sh/) (MacOS/Linux)** 
    ```
    brew install openjdk
    ```
    ```
    brew install mariadb
    brew services start mariadb
    ```

<div style="text-align:center"><br><img src="../img/git.png" alt="Git Logo" width=75vw height=75vh /><br></div>

**Clonazione della Repository del Progetto**

Dopo aver installato Java e MariaDB ed essersi accertati di aver impostato la variabile di path **JAVA_HOME**, è possibile clonare la repository del progetto utilizzando il tuo client Git. Il comando per farlo è il seguente:

```
git clone -b develop https://gitlab.com/5ci-li-megghiu/myfidelity-be
```

**Installazione delle Dipendenze del Progetto**

Una volta clonato il progetto, entra nella cartella del progetto e apri il terminale di sistema per installare le dipendenze. Questo include l'installazione delle dependencies di Spring. I comandi per farlo sono i seguenti:

- **Installazione delle dipendenze di Spring Boot**

    <div style="text-align:center"><br><img src="../img/maven.png" alt="Maven Logo" width=200vw height=125vh /><br></div>

    Le dipendenze di Spring Boot possono essere gestite attraverso un file **pom.xml** il progetto fa utilizzo di [**Maven**](https://maven.apache.org/) per gestire le dependencies. Dopo aver configurato correttamente gli strumenti necessari, puoi installare le dipendenze con:

    ```
    ./mvnw install
    ```

- **Creazione database necessario per l'esecuzione del progetto**

    Ecco i passaggi per creare il database in MariaDB:

    1. **Avvia MariaDB**: Apri il terminale (o il prompt dei comandi in Windows) e digita `mysql -u root -p`. Premi invio, quindi inserisci la password di root quando richiesto.

    2. **Crea il database**: Una volta entrato nel prompt di MariaDB, digita il seguente comando per creare il database:
        ```sql
        CREATE DATABASE fidelity_db;
        ```
        Premi invio per eseguire il comando.

    3. **Verifica la creazione del database**: Per assicurarti che il database sia stato creato correttamente, puoi elencare tutti i database con il comando:
        ```sql
        SHOW DATABASES;
        ```
    Dovresti vedere "fidelity_db" nell'elenco dei database.

    4. **Uscita**: Quando hai finito, puoi uscire da MariaDB digitando `exit` e premendo invio.

- **Esecuzione del Progetto**

    Per eseguire il progetto sul tuo sistema locale, utilizza il seguente comando:

    ```
    ./mvnw spring-boot:run
    ```

    Questo avvierà il server di sviluppo e potrai visualizzare nel terminale di esecuzione che il server sarà operativo con successo alla porta **8080**.