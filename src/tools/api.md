# API

**Introduzione alle API**

Le API sono scritte in linguaggio Java utilizzando il framework Spring Boot. Inoltre, Liquibase viene utilizzato per l'inizializzazione e la gestione delle migrazioni del database, mentre il driver JDBC di MariaDB viene impiegato per la connessione al database MariaDB.

**API UTILIZZATE:**
- **Api di Autenticazione**

    <div style="text-align:center"><br><img src="../img/auth-api.png" alt="API example" width=600vw height=150vh /><br></div>

    **AuthController** è un controller di Spring Boot che gestisce operazioni relative all'autenticazione come la registrazione e il login degli utenti. <br>
    Fornisce due endpoint principali: uno per la registrazione degli utenti e l'altro per il login degli utenti. Questo controller è mappato su ```/api/auth```

    **Registrare un Utente:**

    - Inviare una richiesta POST a ```/api/auth/register``` con un corpo JSON contenente username, password, nome, cognome, email, data, cellulare.
    <br>
    Ricevere una risposta con i dati dell'utente ed un token, con un codice di stato "200" se la richiesta è andata a buon fine ed un codice di stato "403" se la richiesta non è andata a buon fine 

    **Effettuare il Login di un Utente:**

    - Inviare una richiesta POST a ```/api/auth/login``` con un corpo JSON contenente username e password.<br>
    Ricevere una risposta con i dati dell'utente un token ed un code, un codice di stato "200" se la richiesta è andata a buon fine ed un codice di stato "403" se la richiesta non è andata a buon fine 
    <br>

- **Api di gestione Fidelity Card**

    **FidelityCardController** è un controller di Spring Boot che gestisce operazioni relative alle fidelity card, come ottenere le fidelity card di un utente, ottenere la fidelity card attiva di un utente, impostare punti manualmente e ottenere punti per codice.<br> Questo controller è mappato su ```/api``` 

    **1. Ottenere tutte le Fidelity Card di un Utente:**

    - Inviare una richiesta ```GET``` a ```/api/all-fidelity/{user-id}``` <br>
        Ricevere una risposta JSON con una lista di tutte fidelity card dell'utente specificato.

    **2. Ottenere la Fidelity Card Attiva di un Utente:**

    - Inviare una richiesta ```GET``` a ```/api/fidelity/{user-id}``` <br>
    Ricevere una risposta JSON con la fidelity card attiva dell'utente specificato.
    

    **3. Impostare Punti Manualmente:**

    - Inviare una richiesta ```POST``` a ```/api/set-point-manually``` con un corpo JSON contenente fidelityCardId e points.<br>
    Ricevere una risposta JSON con i dettagli aggiornati della fidelity card.


    **4. Ottenere Punti per Codice:**

    - Inviare una richiesta ```GET``` a ```/api/point/{codice}``` <br>
    Ricevere una risposta JSON con i punti associati al codice specificato.

- **Api di gestione dei punti dellaFidelity Card**

    **PuntiController** è un controller di Spring Boot che gestisce operazioni relative ai punti fedeltà, come aggiungere punti a un account fedeltà. Questo controller è mappato su ```/api/punti```

    **Aggiungere Punti:**

    - Inviare una richiesta ```POST``` a ```/api/punti/add-point``` con un corpo JSON contenente codice  e points.<br>
    - Ricevere una risposta JSON con un messaggio di conferma.


- **Api di gestione degli Utenti**

    **UtenteController** è un controller di Spring Boot che gestisce operazioni relative agli utenti come ottenere i dettagli di un singolo utente e ottenere la lista di tutti gli utenti.<br> Questo controller è mappato su ```/api```

    **1. Ottenere i Dettagli di un Utente per ID:**

    - Inviare una richiesta ```GET``` a ```/api/user/{id}``` <br>
    Ricevere una risposta JSON con i dettagli dell'utente specificato

    **2. Ottenere la Lista di Tutti gli Utenti:**

    - Inviare una richiesta ```GET``` a ```/api/user-list``` <br>
    Ricevere una risposta JSON con la lista di tutti gli utenti
    
- **API Qr Code**

La servlet <br> ```https://api.qrserver.com/v1/create-qr-code/?data=${this.codice}&size=360x360&margin=30``` <br> è un servizio esterno che genera codici QR a partire da un testo fornito. In questo caso, l'app MyFidelity utilizza questa servlet per creare un codice QR basato su un input di testo specifico.

Ecco come funziona:

1. **Input del Testo**: L'app MyFidelity fornisce un testo (ad esempio, un URL, un messaggio o un identificatore) che deve essere convertito in un codice QR.

2. **Parametri della Servlet**:
   - `data`: Il testo fornito viene passato come parametro nella parte finale dell'URL. Ad esempio, `${this.codice}` rappresenta il testo da convertire.
   - `size`: Specifica la dimensione del codice QR (in pixel). Nel caso specifico, è impostato su 360x360.
   - `margin`: Specifica il margine intorno al codice QR (in pixel). Qui è impostato su 30.

3. **Generazione del Codice QR**: La servlet "https://api.qrserver.com/v1/create-qr-code/" riceve il testo fornito e crea un'immagine del codice QR corrispondente con le dimensioni e i margini specificati.

4. **Esempio di Richiesta POST**:
   - Per generare un codice QR tramite una richiesta POST, l'app MyFidelity dovrebbe inviare una richiesta HTTP POST all'URL della servlet con il testo da convertire come corpo della richiesta.
   - L'app potrebbe inviare una richiesta POST simile a questa:
     ```http
     POST https://api.qrserver.com/v1/create-qr-code/
     Content-Type: application/x-www-form-urlencoded

     data=testo
     &size=360x360
     &margin=30
     ```
     Dove `testo` rappresenta il testo da trasformare nel codice QR.

     **Risultato prodotto**
      <div style="text-align:center"><br><img src="../img/qrresult.png" alt="Codice QR Prodotto"/></div>

5. **Salvataggio dell'Immagine**: Una volta ottenuta l'immagine del codice QR dalla servlet, l'app MyFidelity può utilizzarla direttamente nell'interfaccia utente.



