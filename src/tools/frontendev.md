# Sviluppo del frontend

Per sviluppare sul progetto dell'App MyFidelity sul lato Frontend, è necessario seguire una serie di passaggi che includono l'installazione di Node.js e delle sue dipendenze, la clonazione della repository del progetto e l'installazione delle dipendenze del progetto. Di seguito sono riportati i passaggi dettagliati:

<div style="text-align:center"><br><img src="../img/nodejs.png" alt="Nodejs Logo" width=75vw height=80vh /><br></div>

**Installazione di [Node.js](https://nodejs.org/en)**

Prima di tutto, è necessario installare Node.js sul tuo sistema. Puoi farlo utilizzando i seguenti comandi a seconda del tuo sistema operativo:

<div style="text-align:center"><br><img src="../img/winos.png" alt="Winget Logo" width=75vw height=75vh /><br></div>

- **[Winget](https://winstall.app/) (Windows 10/11)**
    ```
    winget install --id=OpenJS.NodeJS  -e
    ```

<div style="text-align:center"><br><img src="../img/brewpm.png" alt="Homebrew Logo" width=200vw height=125vh /><br></div>

- **[Homebrew](https://formulae.brew.sh/) (MacOS/Linux)** 
    ```
    brew install node
    ```

<div style="text-align:center"><br><img src="../img/git.png" alt="Git Logo" width=75vw height=75vh /><br></div>

**Clonazione della Repository del Progetto**

Dopo aver installato Node.js, è possibile clonare la repository del progetto utilizzando il tuo client Git. Il comando per farlo è il seguente:

```
git clone -b develop https://gitlab.com/5ci-li-megghiu/pantacard-frontend
```

**Installazione delle Dipendenze del Progetto**

Una volta clonato il progetto, entra nella cartella del progetto e apri il terminale di sistema per installare le dipendenze. Questo include l'installazione di Angular e Ionic, oltre alle dipendenze specifiche del progetto. I comandi per farlo sono i seguenti:

- **Installazione Angular**
    ```
    npm install angular
    ```

- **Installazione Ionic**
    ```
    npm install ionic
    ```

- **Installazione Dipendenze Progetto**
    ```
    npm install
    ```

Dopo aver installato le dipendenze, sarai in grado di partecipare allo sviluppo del progetto.

**Esecuzione del Progetto**

Per eseguire il progetto sul tuo sistema locale, utilizza il seguente comando:

```
ionic serve
```

Questo avvierà il server di sviluppo e potrai visualizzare l'app nel tuo browser.