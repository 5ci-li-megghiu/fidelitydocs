# Backend

**Dettagli Tecnici dello Sviluppo Back-End di MyFidelity**

Nella realizzazione della parte back-end dell’applicazione MyFidelity, sono stati selezionati strumenti adeguati per garantire un funzionamento efficiente e scalabile. Ecco i dettagli tecnici:

**Tecnologie Utilizzate**

<div style="text-align:center"><br><img src="../img/Java-Logo.png" alt="Java Logo" width=260vw height=150vh /><br></div>

- [**Java**](https://www.oracle.com/java/): Il linguaggio di programmazione principale utilizzato per sviluppare la parte backend, tra cui le API usato nella versione 21.
- [**JDBC (Java Database Connectivity)**](https://docs.oracle.com/javase/8/docs/technotes/guides/jdbc/): Un'API Java che definisce come un client può accedere a un database. Utilizziamo il driver JDBC di **MariaDB** per interfacciarci con il database **MariaDB**.

<div style="text-align:center"><br><img src="../img/mariadb.png" alt="MariaDB Logo" width=260vw height=75vh /><br></div>

- [**MariaDB**](https://mariadb.org/): MariaDB, un database relazionale [**open source**](https://it.wikipedia.org/wiki/Open_source), offre elevate prestazioni, sicurezza e compatibilità con MySQL. Il driver JDBC di MariaDB consente alle applicazioni Java di interagire con il database. 

<div style="text-align:center"><br><img src="../img/spring-boot logo.png" alt="Spring Logo" width=300vw height=150vh /><br></div>

- [**Spring Boot**](https://spring.io/projects/spring-boot): Un framework che semplifica la creazione di applicazioni stand-alone e basate su produzione, utilizzando il modello di programmazione Spring.

**Panoramica del Framework Spring Boot**
Spring Boot semplifica la configurazione delle applicazioni Java, supporta lo sviluppo di microservizi e offre un ambiente di sviluppo integrato con server web incorporati.

Alcune delle dependencies utilizzate:

- [**Spring Data JPA**](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#reference): Per l'accesso ai dati tramite **JPA** (Java Persistence API). Semplifica l'interazione con il database e offre funzionalità avanzate per le operazioni **CRUD**.

- [**Spring Boot Starter Web**](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#boot-features-developing-web-applications)
: Per la creazione di servizi web **RESTful**. Questa dipendenza abilita il framework **Spring MVC** per gestire le richieste HTTP e le risposte.

- [**Spring Security Core e Spring Security Web**](https://docs.spring.io/spring-security/site/docs/current/reference/html5/): Per la gestione dell'autenticazione e dell'autorizzazione. Queste dipendenze consentono di proteggere le risorse dell'applicazione e gestire gli utenti.

- [**Lombok**](https://projectlombok.org/features/all): Per la generazione automatica di codice boilerplate. Semplifica la scrittura di classi riducendo la verbosità del codice.

- [**JSON Web Token (JWT)**](https://github.com/jwtk/jjwt): Per la gestione dei token di autenticazione. Questa libreria consente di creare, verificare e gestire i **token JWT**.

<div style="text-align:center"><br><img src="../img/liquibase.png" alt="Liquibase Logo" width=290vw height=180vh /><br></div>

- [**Liquibase**](https://www.liquibase.com/): Uno strumento di gestione delle versioni del database, che consente di tracciare, gestire e applicare modifiche allo schema del database in modo sicuro e affidabile. Utilizzata come dependency all'interno di Spingboot tramite **Liquibase Core**.

**Inizializzazione del Database con Liquibase**
Liquibase gestisce le migrazioni del database, traccia le modifiche e facilita l'integrazione continua automatizzando le migrazioni come parte del processo di build e deploy.

# Struttura Database

Qui troviamo le tabelle e le entità che le compongono con le loro rispettive associazioni, ogni tabella ha un ruolo specifico:

**utenti**: Questa tabella contiene informazioni sugli utenti del sistema. Ogni utente ha un **id**, nome, cognome, email, password, data di nascita, ruolo (per distinguere tra diversi tipi di utenti come clienti, e **admin**), numero di cellulare, username e codice.

**fidelity_card**: Questa tabella tiene traccia delle carte fedeltà emesse. Ogni carta ha un **id**, una data di inizio e di fine validità, e l’id_utente del titolare della carta. L’id_utente è una chiave esterna che fa riferimento all’id nella tabella degli utenti.

**punti**: Questa tabella registra i punti guadagnati dagli utenti. Ogni record ha un **id**, un timestamp (ts) che indica quando i punti sono stati guadagnati, l’importo dei punti e l’id_fidelity della carta fedeltà a cui i punti sono stati accreditati. L’id_fidelity è una chiave esterna che fa riferimento all’id nella tabella delle fidelity_card.

Queste tabelle permettono di gestire un sistema di carte fedeltà, tenendo traccia degli utenti, delle loro carte e dei punti che accumulano. Le chiavi esterne (id_utente e id_fidelity) permettono di collegare le tabelle tra loro, creando relazioni tra gli utenti, le loro carte fedeltà e i punti che guadagnano.

**Modello Concettuale**

| punti          | fidelity_card | utenti      |
| -------------- | ------------- | ----------- |
| id **PK**      | id **PK**     | id **PK**   |
| ts             | data_inizio   | nome        |
| importo        | data_fine     | cognome     |
| id_fidelity **FK** | id_utente **FK**  | email       |
| note           | tot_punti     | password    | 
|                | attiva        | data        |
|                |               | ruolo       |
|                |               | cellulare   |
|                |               | username **UN** |
|                |               | codice **UN**   |



**Modello Logico**

punti(**id**, ts, importo, <ins>id_fidelity</ins>)

fidelity_card(**id**, data_inizio, data_fine, <ins>id_utente</ins>)

utenti(**id**, nome, cognome, email, password, data, ruolo, cellulare, **username**, **codice**)