# Frontend

**Dettagli Tecnici dello Sviluppo Front-End di MyFidelity**

Per la realizzazione della parte front-end dell'applicazione MyFidelity, sono stati selezionati strumenti all'avanguardia nel panorama dello sviluppo web e mobile:

<div style="text-align:center"><br><img src="../img/angular.png" alt="Angular Logo" width=250vw height=70vh /></div>

- [**Angular Typescript**](https://www.typescriptlang.org/docs/handbook/angular.html): Questo potente framework web è stato scelto per la sua capacità di costruire applicazioni dinamiche e reattive. Angular offre un ambiente robusto per lo sviluppo di interfacce utente complesse, sfruttando il linguaggio TypeScript per garantire tipizzazione statica e oggetti orientati agli oggetti.

<div style="text-align:center"><br><img src="../img/ionic.png" alt="Ionic Logo" width=250vw height=85vh /></div>

- [**Ionic Framework**](https://ionicframework.com/docs/angular/overview): La scelta di Ionic come framework cross-platform permette a MyFidelity di essere accessibile su una vasta gamma di dispositivi. Ionic si integra perfettamente con Angular, fornendo componenti UI ottimizzati per dispositivi mobili e una performance fluida su sia iOS che Android.

<div style="text-align:center"><br><img src="../img/akita.png" alt="Akita Logo"/></div>

- [**Akita State Management**](https://github.com/salesforce/akita): Per gestire lo stato dell'applicazione in modo efficiente, è stato adottato Akita. Questo pattern di state management consente di mantenere una fonte di verità unica per lo stato dell'applicazione, facilitando la sincronizzazione dei dati tra le varie parti dell'interfaccia utente.

L'architettura dell'applicazione segue gli standard di Ionic, con una gestione dei dati dell'utente in locale che assicura rapidità e sicurezza nell'accesso alle informazioni. Akita viene utilizzato per gestire lo stato dell'applicazione, offrendo una soluzione scalabile e manutenibile.

```
src/
╰── app/
╰── assets/
╰── environments/
╰── theme/
╰── global.scss
╰── index.html
╰── main.ts
╰── polyfills.ts
╰── test.ts
╰── zone-flags.ts
```

**Struttura delle Pagine:**

- **Homepage Cliente**: La pagina principale del cliente è il cuore dell'esperienza utente, dove è possibile visualizzare la propria fidelity card con i punti accumulati, l'importo e la data degli acquisti qualificanti.

```
app/
╰── tabs/
    ╰── page/
        ╰── home/

```

- **Dashboard Admin**: La homepage dedicata al gestore del negozio offre una panoramica delle statistiche generali del programma di fidelizzazione, inclusa una lista dei clienti con i relativi punti. Questa pagina fornisce anche strumenti per l'analisi statistica, permettendo al negoziante di valutare l'efficacia delle strategie di fidelizzazione.

```
app/
╰── tabs/
    ╰── page/
        ╰── admin/
            ╰── home-admin/
        
```

- **Sezione Autenticazione**: L'applicazione include una sezione dedicata al login e alla registrazione. L'accesso alle funzionalità di fidelizzazione è protetto da autenticazione, assicurando che ogni utente sia univocamente identificato e il suo profilo sia accuratamente gestito.

Per la **registrazione**:

```
app/
╰── page/
    ╰── register/
        
```

Per il **login**:

```
app/
╰── page/
    ╰── login/
        
```