# Introduzione

<div style="text-align:center"><img src="./img/MyFidelity-logo.png" alt="MyFidelity Logo"/></div>

**MyFidelity: Un Sistema Innovativo di Gestione della Fidelizzazione**

MyFidelity è un'applicazione progettata per rafforzare il legame della fiducia tra il negozio e la loro clientela. Attraverso l'uso di questo applicativo, il proprietario del negozio può offrire un'esperienza personalizzata e gratificante ai loro clienti fedeli tramite dei vantaggi forniti da MyFidelity.

**Funzionalità Principali:**

- **Scansione QR**: Ogni cliente possiede un **codice QR univoco**, che il negoziante può facilmente scansionare utilizzando un dispositivo mobile munito del suo accesso predefinito. Questo processo semplifica l'aggiunta di punti fedeltà al profilo del cliente.

<div style="text-align:center"><br><img src="./img/qrcode.png" alt="Pagina test codice QR"/></div>

- **Accumulo Punti**: I clienti accumulano punti fedeltà ogni volta che effettuano acquisti che superano una soglia minima di spesa.

<div style="text-align:center"><br><img src="./img/points.png" alt="Punti accumulati"/></div>

- **Massimo Punti**: Il numero massimo di punti che un cliente può accumulare è **12**. Questi punti non scadono, garantendo ai clienti la libertà di utilizzarli senza preoccupazioni di perdere i benefici acquisiti.

- **Riscatto Punti**: Una volta che un cliente decide di utilizzare i suoi punti massimi accumulati per ottenere uno sconto, il saldo dei punti si azzera. Questo permette al cliente di iniziare un nuovo ciclo di accumulo e di beneficiare nuovamente delle ricompense offerte dalla fidelity card.

- **Auto-Lettura Punti**: I clienti possono monitorare autonomamente i punti fedeltà accumulati attraverso l'applicazione utente. Questa funzione di auto-lettura fornisce trasparenza e consente ai clienti di essere sempre aggiornati sul loro saldo punti.

- **Interfaccia Negozianti**: Esiste un'interfaccia dedicata ai negozianti all'interno dell'applicazione, non accessibile ai clienti normali. Questa interfaccia permette ai negozianti di gestire con facilità le fidelity card, visualizzare i saldi punti di ciascun cliente e monitorare l'efficacia del programma di fidelizzazione.

<div style="text-align:center"><br><img src="./img/admin.png" alt="Dashboard Admin"/></div>

**Vantaggi per il Negozio:**

- **Fidelizzazione Clienti**: L'App MyFidelity incentiva i clienti a tornare, aumentando la probabilità di acquisti ripetuti.

- **Marketing Personalizzato**: Con le informazioni raccolte, i negozianti possono creare offerte personalizzate che rispondono alle preferenze di acquisto dei clienti.

- **Analisi Dati**: L'interfaccia del negoziante fornisce dati preziosi sul comportamento di acquisto dei clienti, permettendo di ottimizzare le strategie di vendita.

**Vantaggi per il Cliente:**

- **Ricompense Tangibili**: I clienti si sentono apprezzati e ricompensati per la loro fedeltà, incentivandoli a continuare a fare acquisti nel negozio.

- **Facilità d'Uso**: L'applicazione è intuitiva e facile da usare, rendendo semplice per i clienti tenere traccia dei loro punti e dei vantaggi.