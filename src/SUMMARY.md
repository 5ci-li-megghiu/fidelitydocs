# Summary

[Introduzione](./introduction.md)

# Utilizzo

- [Cliente](./user/client.md)
- [Admin](./user/admin.md)

# Strumenti utilizzati

- [Frontend](./tools/frontend.md)
    - [Sviluppo](./tools/frontendev.md)
- [Backend](./tools/backend.md)
    - [API](./tools/api.md)
    - [Sviluppo](./tools/backendev.md)