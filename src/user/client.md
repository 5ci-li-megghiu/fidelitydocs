# Utilizzo cliente

**Processo di Installazione e Registrazione nell'App MyFidelity**

Per iniziare a sfruttare i vantaggi dell'app MyFidelity, il cliente dovrà seguire questi passaggi:

1. **Installazione dell'App**: Scaricare e installare l'applicativo MyFidelity per il proprio dispositivo mobile.

2. **Creazione del Profilo Utente**:
   - Accedere alla **pagina di registrazione** integrata nell'app.
   - Compilare i campi richiesti con le proprie informazioni personali.
   - Confermare la registrazione per creare un **profilo** sicuro all'interno dell'app.

   <div style="text-align:center"><br><img src="../img/register.png" alt="Pagina Registrazione"/></div>

3. **Accesso utente**:
   - Effettuare il login con le credenziali scelte in fase di registrazione. Da qui viene possibile la visualizzazione dei dettagli di come i punti sono stati guadagnati, inclusi l'importo speso e la data dell'acquisto.

   <div style="text-align:center"><br><img src="../img/login.png" alt="Pagina Login"/></div>

4. **Accumulo Punti**:
   - Una volta effettuato l'accesso, gli utenti possono iniziare ad accumulare punti fedeltà. I punti verranno registrati digitalmente nel profilo utente ad ogni acquisto qualificante.

5. **Profilo Utente e Codice QR**:
   - Nella sezione profilo, gli utenti possono trovare le loro informazioni personali e la possibilità di disconnettersi dall'App (Logout).
   
   <div style="text-align:center"><br><img src="../img/profile.png" alt="Pagina Profilo"/><br></div>
   
   - In basso alla schermata è presente un icona da dove sarà possibile aprire l'interfaccia di visualizzazione del proprio **Codice QR**.
   - Presentando questo codice QR al negoziante presso i negozi supportati, sarà possibile accumulare punti fedeltà con facilità.

**Esperienza Utente Post-Registrazione**

Dopo la registrazione, l'utente avrà accesso a una serie di funzionalità che migliorano l'esperienza di acquisto:

- **Visualizzazione delle Fidelity Card**: L'utente può consultare le sue fidelity card, sia quelle correnti che quelle passate, con tutti i dettagli relativi agli acquisti effettuati.

- **Gestione dei Punti Fedeltà**: Monitorare i punti accumulati e comprendere come sfruttarli al meglio per ottenere premi o sconti.

- **Facilità di Accumulo Punti**: Grazie al codice QR univoco, l'accumulo dei punti diventa un processo semplice e veloce durante la visita ai negozio affiliato.