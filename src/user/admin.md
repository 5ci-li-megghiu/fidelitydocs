**Utilizzo dell'applicazione con profilo Negoziante/Amministrativo**

L'applicazione MyFidelity offre funzionalità amministrative per gestire le fidelity card e i clienti. Ecco come utilizzarla:

1. **Installazione dell'App**:
   - Scarica e installa l'applicativo MyFidelity sul tuo dispositivo mobile.

2. **Accesso tramite profilo amministrativo**:
   - Accedi all'app utilizzando le credenziali del profilo amministrativo (queste vengono predisposte per il negoziante).

   <div style="text-align:center"><br><img src="../img/adminlogin.png" alt="Pagina Login Admin"/></div>

3. **Scannerizza il codice QR del cliente**:
   - Una volta entrati nell'app, utilizza la funzione di scansione nella parte inferiore della schermata per accedere rapidamente alle informazioni del cliente e registrare un punto sulla fidelity card.

   <div style="text-align:center "><br><img src="../img/scanner-qrcode.jpeg" alt="Pagina Scan QR Admin" width="380vw" height="810vh" /></div>

**Altre funzionalità dell'applicativo nel profilo amministratore**

4. **Visualizzazione delle informazioni sulle fidelity card**:
   - Per vedere il numero di fidelity card attive, vai alla sezione corrispondente nell'app.
   - Controlla anche quante fidelity card sono state completate dai clienti.

   <div style="text-align:center"><br><img src="../img/adminui2.png" alt="Interfaccia punti utenti"/></div>

5. **Gestione dei punti dei clienti**:
   - Visualizza e modifica i punti accumulati da ciascun cliente.

   <div style="text-align:center"><br><img src="../img/adminui.png" alt="Interfaccia modifica punti"/></div>