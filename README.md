# Documentazione MyFidelity

I seguenti sono procedimenti per eseguire in locale per lo sviluppo l'mdBook.

## Installazione 

**Binaries precompilati**

Gli eseguibili sono disponibili per il download sulla pagina GitHub Releases, per la tua piattaforma (Windows, MacOS o Linux) ed estrae l'archivio. L'archivio contiene un libro genealogico eseguibile che puoi correre per costruire gli mdBook.

Per facilitare l'esecuzione, mettere il percorso verso il file eseguibile nel PATH.

**Rust**

Per costruire il mdbook eseguibile dalla fonte, sarà necessario installare Rust e Cargo.
- [Download Rust](https://www.rust-lang.org/tools/install)

Una volta installato Rust, il seguente comando può essere usato per costruire e installare mdBook:

`cargo install mdbook`

Questo scarica automaticamente mdBook da crates.io e lo installa nella directory binaria globale di Cargo **(~/.cargo/bin/ per default).**

## Build mdBook

Entrare nella directory del libro ed eseguire il seguente comando per buildare:

`mdbook build`

e 

`mdbook serve`

per l'esecuzione.